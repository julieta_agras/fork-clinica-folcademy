package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @GetMapping("")
    public ResponseEntity<List<MedicoEnteroDto>> listarTodo(){
        return ResponseEntity.ok(medicoService.listarTodos());
    }

    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> listarUno(@PathVariable(name ="idMedico")int id){
        return ResponseEntity.ok(medicoService.listarUno(id));
    }
    @PostMapping("")
    public ResponseEntity<MedicoEnteroDto> agregar(@RequestBody @Validated MedicoEnteroDto entity ){
        return ResponseEntity.ok(medicoService.agregar(entity));
    }
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.editar(id,dto));
    }
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id){
        return ResponseEntity.ok(medicoService.eliminar(id));
    }
}
