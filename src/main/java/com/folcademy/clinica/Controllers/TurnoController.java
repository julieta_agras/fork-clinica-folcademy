package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/turnos")
public class TurnoController {
    private final TurnoService turnoService;
    public TurnoController (TurnoService turnoService){
        this.turnoService= turnoService;
    }
   /* @GetMapping(value = "")
    public ResponseEntity<List<Turno>> findAll(){
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAllTurnos()
                );
    }*/

    @GetMapping("")
    public ResponseEntity<List<TurnoDto>> listarTodo(){
        return ResponseEntity.ok(turnoService.listarTodo());
    }

    @GetMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> listarUno(@PathVariable(name ="idTurno")int id){
        return ResponseEntity.ok(turnoService.listarUno(id));
    }
    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto dto ){
        return ResponseEntity.ok(turnoService.agregar(dto));
    }
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> editar(@PathVariable(name = "idTurno") int id,
                                                  @RequestBody TurnoDto dto){
        return ResponseEntity.ok(turnoService.editar(id,dto));
    }
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idTurno") int id){
        return ResponseEntity.ok(turnoService.eliminar(id));
    }

}
