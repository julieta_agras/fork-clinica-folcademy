package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {
    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @GetMapping(value = "")
    public ResponseEntity<List<Paciente>> findAll() {
        return ResponseEntity.ok().body(pacienteService.findAllPacientes());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<Paciente>> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity.ok().body(pacienteService.findPacienteById(id));
    }

    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.agregar(dto));
    }

    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }

    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDto> editar(@PathVariable(name = "idPaciente") Integer id,
                                              @RequestBody PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.editar(id, dto));

    }
}

