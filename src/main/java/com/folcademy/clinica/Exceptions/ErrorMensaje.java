package com.folcademy.clinica.Exceptions;

public class ErrorMensaje {
    private String message;
    private String detall;
    private String code;
    private String path;
    public ErrorMensaje(String message,String detall, String code, String path){
        this.message = message;
        this.detall = detall;
        this.code = code;
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public String getDetall() {
        return detall;
    }

    public String getCode() {
        return code;
    }

    public String getPath() {
        return path;
    }

}
