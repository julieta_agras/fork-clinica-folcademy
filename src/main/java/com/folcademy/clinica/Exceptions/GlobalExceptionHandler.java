package com.folcademy.clinica.Exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<ErrorMensaje> defaultErrorHandler(HttpServletRequest req, Exception e){
        return new ResponseEntity<ErrorMensaje>(new ErrorMensaje("Error Genérico",e.getMessage(),"1",req.getRequestURI()), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMensaje> notFoundHandler(HttpServletRequest req,Exception e){
        return new ResponseEntity<ErrorMensaje>(new ErrorMensaje("Not found", e.getMessage(), "2", req.getRequestURI()), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<ErrorMensaje> badRequestHandler(HttpServletRequest req,Exception e){
        return new ResponseEntity<ErrorMensaje>(new ErrorMensaje("Bad Request", e.getMessage(), "3", req.getRequestURI()), HttpStatus.BAD_REQUEST);
    }


}
