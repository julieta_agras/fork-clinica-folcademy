package com.folcademy.clinica.Model.Mappers;


import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
   public MedicoEnteroDto entityToDto(Medico entity){
       return Optional
               .ofNullable(entity)
               .map(
                       ent -> new MedicoEnteroDto(
                               ent.getIdmedico(),
                               ent.getNombre(),
                               ent.getApellido(),
                               ent.getProfesion(),
                               ent.getConsulta()
                       )

               )
               .orElse(new MedicoEnteroDto());
   }
   public Medico dtoToEntity(MedicoEnteroDto dto) {
      Medico entity = new Medico();
      entity.setIdmedico(dto.getId());
      entity.setNombre(dto.getNombre());
      entity.setApellido(dto.getApellido());
      entity.setProfesion(dto.getProfesion());
      entity.setConsulta(dto.getConsulta());
      return entity;
   }
}

