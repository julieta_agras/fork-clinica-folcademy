package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class TurnoDto {
    Integer idTurno;
    LocalDate fecha;
    LocalTime hora;
    Boolean atendido;
    Integer idpaciente;
    Integer idmedico;
}
