package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this. turnoMapper= turnoMapper ;
    }
    public List<TurnoDto> listarTodo(){
        return turnoRepository.findAll().stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
    }
    public TurnoDto listarUno(Integer id){
        if(id<0) {
            throw new BadRequestException("La busqueda que realizó no existe");
        }
        return turnoRepository.findById(id).map(turnoMapper::entityToDto).orElse(null);

    }
    public TurnoDto agregar(TurnoDto dto){
        if ((dto.getIdmedico() == null) || (dto.getIdpaciente() == null)){
            throw new BadRequestException("No se pudo agregar turno");
        } else {
            dto.setIdTurno(null);
            return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
        }

    }
    public TurnoDto editar (int id, TurnoDto dto){
        if (!turnoRepository.existsById(id))
            throw new RuntimeException("No existe el turno");
        dto.setIdTurno(id);
        return
                turnoMapper.entityToDto(
                        turnoRepository.save(
                                turnoMapper.dtoToEntity(
                                        dto
                                )
                        )
                );
    }

    public Boolean eliminar(int id){
        if (!turnoRepository.existsById(id))
            throw new NotFoundException("No se encontró el turno que se desea eliminar");
        turnoRepository.deleteById(id);
        throw new BadRequestException("Se eliminó correctamente");
    }

    @Override
    public List<Turno> findAllTurnos() {
        return turnoRepository.findAll();
    }

    @Override
    public List<Turno> findTurnoById(Integer id) {
        List<Turno> lista = new ArrayList<>();
        Turno turno = turnoRepository.findById(id).get();
        lista.add(turno);
        return lista;
    }
}
