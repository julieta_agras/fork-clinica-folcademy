package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.ErrorMensaje;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service ("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }
    public List<MedicoEnteroDto> listarTodos(){
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }
    public MedicoEnteroDto listarUno(Integer id){
        if(id<0) {
            throw new BadRequestException("La busqueda que realizó no existe");
        }
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);

    }
    public MedicoEnteroDto agregar(MedicoEnteroDto entity){
        if ((entity.getNombre() == null) || (entity.getApellido() == null)){
            throw new BadRequestException("No se pudo agregar medico");
        } else {
            entity.setId(null);
            return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
        }

    }
    public MedicoEnteroDto editar (int idMedico, MedicoEnteroDto dto){
        if (!medicoRepository.existsById(idMedico))
            throw new RuntimeException("No existe el medico");
        dto.setId(idMedico);
        return
                medicoMapper.entityToDto(
                        medicoRepository.save(
                                medicoMapper.dtoToEntity(
                                        dto
                                )
                        )
                );
    }

    public Boolean eliminar(int id){
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("No se encontró el médico que se desea eliminar");
        medicoRepository.deleteById(id);
        return true;
    }

}

