package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    @Override
    public List<Paciente> findAllPacientes() {
        if (pacienteRepository.findAll() != null) {
            throw new NotFoundException("No se encontró ningún paciente");
        }else {
            return (List<Paciente>) pacienteRepository.findAll();
        }
    }


    @Override
    public List<Paciente> findPacienteById(Integer id) {
        List<Paciente> lista = new ArrayList<>();
        Paciente paciente = pacienteRepository.findById(id).get();
        lista.add(paciente);
        return lista;
    }

    public PacienteDto agregar(PacienteDto dto) {
        if ((dto.getDni() == null) & (dto.getNombre() == null) & (dto.getApellido() == null))
            throw new NotFoundException("No pudo agregarse");
        dto.setId(null);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(dto)));
    }




    public Boolean eliminar(int id){
        if (!pacienteRepository.existsById(id))
            return false;
        pacienteRepository.deleteById(id);
        return true;
    }
    public PacienteDto editar(int idPaciente,PacienteDto dto){
        if (!pacienteRepository.existsById(idPaciente))
            return null;
        dto.setId(idPaciente);
        return
                pacienteMapper.entityToDto(
                        pacienteRepository.save(
                                pacienteMapper.dtoToEntity(dto
                                )
                        )
                );
    }
}